from os import stat
import pandas as pd
import pickle
import re
import unicodedata
import statistics
import math

# Fonctions de pré-traitement


def remove_links(txt):
    """
    enlève les liens

        txt : String
    """
    pattern = '([a-z]+://\S+)|(www.\S+)'
    return re.sub(pattern, ' ', txt)


def remove_quotes(txt):
    """
    enlève les tweets cités

        txt : String
    """
    pattern = 'RT\s@.*'
    return re.sub(pattern, '', txt)


def normalize_spaces(txt):
    """
    remplace les longues chînes d'espaces par un simple espace

        txt : String
    """
    pattern = '\s+'
    return re.sub(pattern, ' ', txt)


def remove_accents(txt):
    """
    enlève les accents

        txt : String
    """
    return ''.join((c for c in unicodedata.normalize('NFD', txt) if unicodedata.category(c) != 'Mn'))


def remove_special_chars(txt):
    """
    enlève les caractères spéciaux

        txt : String
    """
    # On veut garder les smileys
    pat = ':\)+|:D+|:\(+(?:\s|$)'
    smileys = re.findall(pat, txt)
    txt = re.sub(pat, ' ', txt)

    # Passage en ASCII
    en = txt.encode('ascii', 'ignore')
    txt = en.decode('unicode_escape')

    # Virer les @ et # qui ne sont pas des mentions/hashtags
    pat = '(@|#)(?!\w+)'
    txt = re.sub(pat, ' ', txt)

    # Virer les caratères spéciaux
    txt = ''.join([l for l in txt if l.isalnum() or l in (' ', '#', '@')])

    return txt + ' '.join(smileys)


def preprocess_tweet(txt):
    """
    Effectue toutes les étapes de pré-traitement sur un tweet

        txt : String
    """
    return normalize_spaces(remove_special_chars(remove_accents(remove_links(remove_quotes(str(txt)))))).lower()


# Affichage statistique

def mean_tweet_size(df, key='text'):
    """
    Calcule la taille moyenne des tweets dans une base de données

        df : pandas.DataFrame
    """
    sizes = []

    for entry in df[key]:
        sizes.append(len(entry))

    return int(statistics.mean(sizes))


# Apprentissage

def tokenize(doc):
    return doc.split(' ')


def inverted_index(df):
    """
    Donne l'index inversé de df qui a une collection de tweets dans df['text']
    Si 'duplicate' est vrai, on compte un terme autant de fois qu'il apparait dans un doc

        df : pandas.Dataframe
        duplicate : bool

        return_type : dict
    """
    tweets = df['text']
    n_tweets = len(df)

    index = {}
    for i in range(n_tweets):
        twt = tweets[i]
        for t in tokenize(twt):
            if t in index:
                index[t].append(i)
            else:
                index[t] = [i]

    return index


def pond_tf_idf(N, n_d, tf_t_d, df_t):
    """
    Fait le calcul d'une pondération tf-idf étant donnés les données néessaire

        N,n_d,tf_t_d,df_t : int
    """
    if tf_t_d == 0 or df_t == 0:
        return 0
    else:
        return (1+math.log10(tf_t_d))*math.log10(N/df_t)


def tweet2vec(doc, doclist, index, pond=pond_tf_idf):
    """
    Donne la représentation vectorielle d'un document étant donnée la liste des docs d'apprentissag, l'index inversé et un schéma de pondération

        doc : String
        doclist : String list
        index : dict
    """

    term_list = tokenize(doc)
    term_set = set(term_list)

    n_docs = len(doclist)

    tweet_vec = []

    n_d = len(term_list)
    for t in index.keys():
        tf_t_d = term_list.count(t)
        df_t = len(set(index[t]))+1
        tweet_vec.append(pond(n_docs, n_d, tf_t_d, df_t))

    return tweet_vec


def split_data(df, p=0.8):
    """
    Sépare un jeu de données en un training set et un test set

    df : pandas.Dataframe
    """
    shuffled = df.sample(frac=1)
    return df.iloc[:int(len(df)*0.8), :], df.iloc[int(len(df)*0.8):, :]
