import json
import os
import gensim
import numpy as np
import utils
import pandas as pd
from sklearn.svm import LinearSVC, SVC
from sklearn.decomposition import PCA


def save_to_json(my_dict, path="outputs", filename="tweets_backup.json"):
    """
    Sauvegarde le dictionnaire passé en argument au format json
    :param my_dict: Dictionnaire à sauvegarder
    :type my_dict: Dict
    :param path: Path for the json file
    :type path: Str
    :param filename: The name of the json file
    :type filename: Str

    :return: N/A
    """
    if not os.path.exists(path):
        os.makedirs(path)
    with open(path+"/"+filename, "w") as f:
        json.dump(my_dict, f)


def extract_from_json(path="outputs", filename="tweets_backup.json"):
    """
    Exporte vers un dictionnaire les données contenues dans le fichier json spécifié.
    :param path: Path for the json file
    :type path: Str
    :param filename: The name of the json file
    :type filename: Str

    :return: The dictionnary containing the data of the json file
    :rtype: Dict
    """
    with open(path+"/"+filename, "r") as f:
        my_dictionnary = json.load(f)
    return my_dictionnary


def read_json(path_list=["datasets/semeval", "datasets/SS-Twitter", "datasets/STS-Test"], filename="pp.json"):
    data = []
    for path in path_list:
        new_data = extract_from_json(path, filename)
        data = data + new_data
    train_data = []
    test_data = []
    i = 0
    limit = 9.5 * len(data) / 10
    for element in data:
        if i < limit:
            text = gensim.utils.simple_preprocess(element["text"])
            train_data.append(gensim.models.doc2vec.TaggedDocument(
                text, [i, element["labels"]]))
        else:
            test_data.append((gensim.utils.simple_preprocess(
                element["text"]), element["labels"]))
        i += 1
    print("train len : {}".format(len(train_data)))
    print("test len : {}".format(len(test_data)))
    return train_data, test_data


def create_d2v_model(train_data, path="models", name="d2v.model", epochs=40, vector_size=50, min_count=2):
    model = gensim.models.doc2vec.Doc2Vec(
        vector_size=vector_size, min_count=min_count, epochs=epochs)
    model.build_vocab(train_data)
    model.train(train_data, total_examples=model.corpus_count,
                epochs=model.epochs)
    if not os.path.exists(path):
        os.makedirs(path)
    model.save(path+"/"+name)
    return model


# Schéma vectoriel à pondération
df_sts = pd.read_json('datasets/STS-Test/pp.json', orient='records')
df_ss = pd.read_json('datasets/SS-Twitter/pp.json', orient='records')
df_sem = pd.read_json('datasets/semeval/pp.json', orient='records')
df = pd.concat([df_sts, df_ss, df_sem], ignore_index=True).iloc[:, :]
print('1')
df_train, df_test = utils.split_data(df, p=0.9)

index = utils.inverted_index(df_train)
print('2')


def vect_trans(doc): return utils.tweet2vec(
    doc=doc, doclist=df_train['text'], index=index)


Xtrain = np.array(list(map(vect_trans, list(df_train['text']))))
Xtest = np.array(list(map(vect_trans, list(df_test['text']))))
Ytrain, Ytest = list(df_train['labels']), list(df_test['labels'])
print('3')

# Doc2Vec
"""
train_data, test_data = read_json()
D2Vmodel = create_d2v_model(train_data, "models", "d2v.model", 40, 750, 2)
"""

#################################################################################################################################
################################################ K neighbors ####################################################################
#################################################################################################################################


def test_model_neighbors(train_data, test_data, name="models/d2v.model", nb_nearest=1, upper_threshold=0, lower_threshold=0):
    model = gensim.models.doc2vec.Doc2Vec.load(name)
    right = 0
    false = 0
    for element, true_positivity in test_data:
        vector = model.infer_vector(element)
        sims = model.dv.most_similar([vector], topn=nb_nearest)
        sum = 0
        for id, _ in sims:
            _, positivity = train_data[id].tags
            sum += positivity

        if sum > upper_threshold:
            answer = 1
        elif sum < lower_threshold:
            answer = -1
        else:
            answer = 0

        if answer == true_positivity:
            right += 1
        else:
            false += 1

    print("There was {} true and {} false answers".format(right, false))
    return right/(right+false)


#success = test_model_neighbors(train_data,test_data,"models/d2v.model",1,0,0)


#################################################################################################################################
################################################ Linear SVC #####################################################################
#################################################################################################################################

def generate_typical_data(path_list=["datasets/semeval", "datasets/SS-Twitter", "datasets/STS-Test"], filename="pp.json"):
    data = []
    for path in path_list:
        new_data = extract_from_json(path, filename)
        data = data + new_data
    Xtrain = []
    Xtest = []
    Ytrain = []
    Ytest = []
    i = 0
    limit = 9.5 * len(data) / 10
    for element in data:
        if i < limit:
            Xtrain.append(D2Vmodel.infer_vector([element["text"]]))
            Ytrain.append(element["labels"])
        else:
            Xtest.append(D2Vmodel.infer_vector([element["text"]]))
            Ytest.append(element["labels"])
        i += 1
    print("train len : {}".format(len(Ytrain)))
    print("test len : {}".format(len(Ytest)))
    return Xtrain, Ytrain, Xtest, Ytest


def create_LSVC_model(Xtrain, Ytrain):
    LSVCmodel = LinearSVC(dual=False)
    LSVCmodel.fit(Xtrain, Ytrain)
    return LSVCmodel


#Xtrain,Ytrain,Xtest,Ytest = generate_typical_data()
LSVCmodel = create_LSVC_model(Xtrain, Ytrain)


def test_model_LSVC(Xtest, Ytest):
    right = 0
    false = 0
    for i in range(len(Xtest)):
        prediction = LSVCmodel.predict(Xtest[i].reshape(1, -1))
        if prediction == Ytest[i]:
            right += 1
        else:
            false += 1
    print("LSVC")
    print("There was {} true and {} false answers".format(right, false))
    return right/(right+false)


success = test_model_LSVC(Xtest, Ytest)


#################################################################################################################################
################################################ SVC ############################################################################
#################################################################################################################################

def create_SVC_model(Xtrain, Ytrain, n_components):
    pca = PCA(n_components=n_components)
    Xreduced = pca.fit_transform(X=Xtrain)

    SVCmodel = SVC()
    SVCmodel.fit(Xreduced, Ytrain)
    return SVCmodel


n_components = 500
SVCmodel = create_SVC_model(Xtrain, Ytrain, n_components)
print('4')


def test_model_SVC(Xtest, Ytest, n_components):
    pca = PCA(n_components=n_components)
    Xreduced = pca.fit_transform(X=Xtest)

    right = 0
    false = 0
    for i in range(len(Xtest)):
        prediction = SVCmodel.predict(Xtest[i].reshape(1, -1))
        if prediction == Ytest[i]:
            right += 1
        else:
            false += 1
    print("SVC")
    print("There was {} true and {} false answers".format(right, false))
    return right/(right+false)


success = test_model_SVC(Xtest, Ytest)
