import pandas as pd
import pickle
import re
import unicodedata
import utils

# ----SS-Twitter----


sstwt_df = pd.read_csv('datasets/SS-Twitter/twitter4242.txt', sep='\t')


# -- Sauvegarde données brutes (marche pas mais osef)


"""
raw_file = open('datasets/SS-Twitter/raw.pickle', 'wb')
pickle.dump(sstwt_df, raw_file)
"""


# -- Pré-traitement

# Normalisation des labels - cf README


mean_pos_list = sstwt_df['mean pos']
mean_neg_list = sstwt_df['mean neg']
labels = []
for i in range(len(mean_pos_list)):
    n, p = int(mean_neg_list[i]), int(mean_pos_list[i])
    r = p/n
    if r == 1:
        labels.append(0)
    elif r >= 1.5:
        labels.append(1)
    else:
        labels.append(-1)


# Traitement du texte


raw_tweets = sstwt_df['Tweet']
pp_tweets = []
for t in raw_tweets:
    pp_tweets.append(utils.preprocess_tweet(t))


# Agrégation


df = pd.DataFrame()
df['labels'] = labels
df['text'] = pp_tweets

df = df[df.text != '']


# Sauvegarde


df.to_json('datasets/SS-twitter/pp.json', orient='records')


# ----STS Test----

sts_df = pd.read_csv('datasets/STS-Test/dataset.csv', sep=',')


# --Sauvegarde des données brutes


# --Pré-traitement

# Labels


labels_pre = sts_df['polarity'].copy()
labels_post = []
for l in labels_pre:
    switcher = {0: -1, 2: 0, 4: 1}
    labels_post.append(switcher.get(l))


# Texte


raw_tweets = sts_df['text']
pp_tweets = []
for t in raw_tweets:
    pp_tweets.append(utils.preprocess_tweet(t))


# Agrégation


df = pd.DataFrame()
df['labels'] = labels_post
df['text'] = pp_tweets

df = df[df.text != '']


# Sauvegarde


df.to_json('datasets/STS-Test/pp.json', orient='records')


# ----SemEval----


semeA_df = pd.read_csv('datasets/semeval/testA.txt', sep='\t')
semeA_df.rename(columns={'negative': 'labels',
                         '\* she photoshops out a gap in her chipped tooth a gap that she can whistle through \*': 'text'}, inplace=True)
semeA_df.dropna(axis=0, inplace=True)

semeB_df = pd.read_csv('datasets/semeval/testA.txt', sep='\t')
semeB_df.rename(columns={
                '\* she photoshops out a gap in her chipped tooth a gap that she can whistle through \*': 'text', 'negative': 'labels'}, inplace=True)
semeB_df.dropna(axis=0, inplace=True)

seme_df = pd.concat([semeA_df, semeB_df])

# --Sauvegarde des données brutes


# --Pré-traitement

# Labels


labels_pre = seme_df['labels'].copy()
labels_post = []
for l in labels_pre:
    switcher = {'negative': -1, 'neutral': 0, 'positive': 1}
    labels_post.append(switcher.get(l))


# Texte


raw_tweets = seme_df['text']
print(len(raw_tweets))
pp_tweets = []
a = True
for t in raw_tweets:
    pp_tweets.append(utils.preprocess_tweet(t))


# Agrégation


df = pd.DataFrame()
df['labels'] = labels_post
df['text'] = pp_tweets

df = df[df.text != '']


# Sauvegarde


df.to_json('datasets/semeval/pp.json', orient='records')
