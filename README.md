# Twitter Sentiment

De nombreux modules sont à installer avant de pouvoir executer toutes les fonctions du repo :
- gensim
- scikit learn
- folium
- pandas
- nltk
- textblob
- tweepy

Le repo est strucuturé de la manière suivante :
- inputs : tous les fichiers que nous entrons en paramètre : fichiers de mots clés
- outputs : toute sortie d'un programme qui est sauvegardée devrait se trouver ici
- datasets : les bases de données sont ici
- models : Les différents modèles entraînés sont supposés être stockés ici

Les fichiers sont présents en deux exemplaires : nous avons travaillé avec des fichiers .py, qui sont présents dans le repo. Cependant, si vous préférez la structure de notebook, notre code a été transposé dans des notebooks en fin de projet, et les notebooks ainsi créés sont à la racine du projet, dans le repo.