from victor_credentials import CONSUMER_KEY, CONSUMER_SECRET, ACCESS_SECRET, ACCESS_TOKEN
import tweepy
import pickle
import os
import json
import time


########################################################################################################################################
################################################## Initialyze Twitter Connexion ########################################################
########################################################################################################################################


def twitter_setup():
    """
    Initie une connexion à l'API Twitter

    :return: Connexion à Twitter
    :rtype: Twitter API Token
    """
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    api = tweepy.API(auth)
    return api


conn = twitter_setup()


########################################################################################################################################
################################################### Get replies (useless atm) ##########################################################
########################################################################################################################################


def get_replies_to_tweet(person_id, tweet):
    """
    Trouve les réponses à un tweet donné du candidat person_id
    :param person_id: Le numéro du candidat concerné : il s'agit de son id Tweeter
    :type person_id: Int
    :param tweet: Le tweet dont on veut trouver les réponses
    :type tweet: Status (tweet résultat d'une requête search)

    :return: La liste des tweets émis en réponse au tweet donné en argument
    :rtype: List
    """
    name = person_id
    tweet_id = tweet.id
    replies = []
    for tweet in tweepy.Cursor(conn.search, q='to:'+name, result_type='recent', timeout=999999).items(10):
        if hasattr(tweet, 'in_reply_to_status_id_str'):
            if (tweet.in_reply_to_status_id_str == tweet_id):
                replies.append(tweet)
    return replies


########################################################################################################################################
################################################### Get tweets by keywords #############################################################
########################################################################################################################################


"""
Format des fichiers de mots clés à placer en entrée : Tous les mots clés en ligne, séparés par des virgules, sur une seule ligne.
NE PAS METTRE D'ESPACES AVANT OU APRES LES VIRGULES !!!!
Pour les hashtag, le symbole # DOIT être inclus dans le document pour chaque entrée
"""


def get_query(path="inputs", is_keyword=False, is_hashtag=False,is_countries=True):
    """
    Renvoie la liste des requêtes associées aux fichiers de mots clés.
    :param path: chemin jusqu'aux fichiers de données
    :type path: Str
    :param is_keyword: Y a-t-il un fichier de mots-clés ?
    :type is_keyword: Bool
    :param is_hashtag: Y a-t-il un fichier de hashtags ?
    :type is_hashtag: Bool
    :param is_hashtag: Y a-t-il un fichier de pays ?
    :type is_hashtag: Bool

    :return: Liste des requêtes à effectuer
    :rtype: List
    """
    try:
        klist = []
        hlist = []
        clist = []
        if is_keyword:
            kfile_name = "keywords.txt"
            with open(path + '/' + kfile_name, 'r') as kfile:
                lignes = kfile.readlines()[0].split(',')
                klist = lignes

        if is_hashtag:
            hfile_name = "hashtags.txt"
            with open(path + '/' + hfile_name, 'r') as hfile:
                lignes = hfile.readlines()[0].split(',')
                hlist = lignes

        if is_countries:
            kfile_name = "countries.txt"
            with open(path + '/' + kfile_name, 'r') as kfile:
                lignes = kfile.readlines()[0].split(',')
                clist = lignes

        wordlist = []
        for country in clist :
            for keyword in klist :
                wordlist.append(country+" "+keyword)
            for hashtag in hlist :
                wordlist.append(country+" "+hashtag)

        return wordlist

    except OSError:
        print('OSError : probably wrong path.')
    except:
        print("unexpected error : probably not a path problem")


def get_tweets_from_queries(queries):
    """
    Récupère les tweets correspodants aux mot-clés entrés en paramètre
    :param queries: La liste de tous les mots clés ou hashtag que l'on doit rechercher dans Twitter
    :type queries: List

    :return: La liste des tweets qui correspondent aux mots clé demandés
    :rtype: List
    """
    tweets = []
    for element in queries:
        tweets_wanted = conn.search(
            "lang:en "+element+"  -filter:retweets", tweet_mode='extended')
        try:
            for tweet in tweets_wanted:
                tweets.append(tweet)
        except tweepy.TweepError:
            print("an error occured on query " + element + ".")
            try:
                print("The error is " + tweepy.TweepError.response.text)
            except:
                pass
        except:
            print("unexpected error occured, probably not directly related to tweepy")
    return tweets


def store_tweets(tweets):
    """
    Récupère tous les tweets de la liste de tweets présentés, dont la langue est l'anglais, et qui ne sont pas des retweets.
    :param tweets: La liste de tous les tweets à examiner
    :type tweets: List

    :return: Dictionnary of all the tweets
    :rtype: Dict
    """
    dic = {}
    for element in tweets:
        if element.lang == "en":
            try:
                _ = element.retweeted_status
            except:
                dic[element.id] = element
        else:
            pass
    return dic


def save_to_json(my_dict, path="outputs", filename="tweets_backup.json"):
    """
    Sauvegarde le dictionnaire passé en argument au format json
    :param my_dict: Dictionnaire à sauvegarder
    :type my_dict: Dict
    :param path: Path for the json file
    :type path: Str
    :param filename: The name of the json file
    :type filename: Str

    :return: N/A
    """
    if not os.path.exists(path):
        os.makedirs(path)
    with open(path+"/"+filename, "w") as f:
        json.dump(my_dict, f)


def extract_from_json(path="outputs", filename="tweets_backup.json"):
    """
    Exporte vers un dictionnaire les données contenues dans le fichier json spécifié.
    :param path: Path for the json file
    :type path: Str
    :param filename: The name of the json file
    :type filename: Str

    :return: The dictionnary containing the data of the json file
    :rtype: Dict
    """
    with open(path+"/"+filename, "r") as f:
        my_dictionnary = json.load(f)
    return my_dictionnary


def concatenate_dict(dict_list):
    """
    Concatène deux dictionnaires, sans répéter les doublons
    :param dict_list: List of all the dictionnaries to concatenate
    :type path: List

    :return: The dictionnary containing all the data of all the previous dictionnaries
    :rtype: Dict
    """
    ref_dict = dict_list[0]
    for i in range(1, len(dict_list)):
        for key, element in dict_list[i].items():
            if key in ref_dict:
                pass
            else:
                ref_dict[key] = element
    return ref_dict


def clean_tweets(my_dict,my_query):
    """
    Deletes some useless data from the tweets
    :param my_dict: Dictionnary of full tweets
    :type path: Dict

    :return: The dictionnary containing the usefull data extracted from the tweets
    :rtype: Dict
    """
    clean_dict = {}
    for key, element in my_dict.items():
        clean_dict[key] = {}

        try:
            clean_dict[key]["text"] = element.extended_tweet.full_text
        except:
            clean_dict[key]["text"] = element.full_text
        clean_dict[key]["query"] = my_query
        clean_dict[key]["place"] = element.user.location
        try :
            clean_dict[key]["country"] = element.place.country
            clean_dict[key]["country_code"] = element.place.country_code
        except :
            clean_dict[key]["country"] = "N/A"
            clean_dict[key]["country_code"] = "N/A"
        clean_dict[key]["retweet_count"] = element.retweet_count
        clean_dict[key]["favorite_count"] = element.favorite_count
        clean_dict[key]["tweet_id"] = element.id

    return clean_dict


def dict_to_list(my_dict):
    """
    Turns a dictionnary into a list. The keys are lost in the process
    :param my_dict: Dictionnary to transform
    :type path: Dict

    :return: The list of the values of the dictionnary. Keys are lost
    :rtype: List
    """
    my_list = []
    for _, element in my_dict.items():
        my_list.append(element)

    return my_list


def list_to_pickle(my_list, path="outputs", filename="tweets_backup_clean.pickle"):
    """
    Exports to a pickle file the data in the given list
    :param my_list: The list to transform into a pickle file
    :type my_list: List
    :param path: Path for the pickle file
    :type path: Str
    :param filename: The name of the pickle file
    :type filename: Str

    :return: N/A
    """
    with open(path+"/"+filename, "wb") as f:
        pickle.dump(my_list, f)



def main_safe(path="inputs", is_keywords=True, is_hashtags=True,is_countries = True):
    """
    Generates a pickle file of all tweets corresponding to the given hashtags and keywords.
    :param path: Path leading to the place where the text files containing hashtags and keywords are.
    :type path: Str
    :param is_keywords: Is there a file containing keywords ?
    :type is_keywords: Bool
    :param is_hashtags: Is there a file containing hashtags ?
    :type is_hashtags: Bool

    :return: N/A
    """
    words_list = get_query(path, is_keywords, is_hashtags,is_countries)
    for word in words_list:
        print("Beginning word : {}".format(word))
        try :
            tweets = get_tweets_from_queries([word])
            dictionnary = store_tweets(tweets)
            clean_dictionnary = clean_tweets(dictionnary,word)
            try:
                global_dict = extract_from_json("outputs", "tweets_backup.json")
            except:
                global_dict = {}
            global_dict = concatenate_dict([global_dict, clean_dictionnary])
            save_to_json(global_dict, "outputs", "tweets_backup.json")
        except BaseException as e:
            print('failed on_status,',str(e)) # print the error code obtained from twitter
            time.sleep(5) # provide a time before resuming the code when an error arises
        print("Last word was {}".format(word))
        time.sleep(10)

    clean_list = dict_to_list(clean_dictionnary)
    list_to_pickle(clean_list, "outputs", "tweets_backup_clean.pickle")



def count_tweets(path="outputs", filename="tweets_backup.json"):
    """
    Rajoute/mets à jour pour le fichier json spécifié sa longueur
    :param path: Path for the json file
    :type path: Str
    :param filename: The name of the json file
    :type filename: Str

    :return: N/A
    """
    my_dict = extract_from_json(path, filename)
    print("The number of collected tweets is {}".format(len(my_dict)))

while True :
    main_safe("inputs",True,False)

    count_tweets()
