Origine : Thelwall, M., Buckley, K., Paltoglou, G.: Sentiment strength detection for the socialweb. Journal of the American Society for Information Science and Technology 63(1),163–173 (2012)

Traitement recommandé :
 - neg = pos -> neutral
 - 1.5 * neg < pos -> positive
 - else -> negative

(Recommandé par Saif, Hassan, et al. "Evaluation datasets for Twitter sentiment analysis: a survey and a new dataset, the STS-Gold." (2013))